﻿using System;
using System.Web;
using System.Web.UI;

namespace Bonus_Assignment
{

    public partial class Default : System.Web.UI.Page
    {
        //===============BONUS QUESTION 1===============//
        public void Button1Clicked(object sender, EventArgs args){
            string xAxis = x_axis.Text.ToString();
            string yAxis = y_axis.Text.ToString();

            string q1 = "Quadrant 1";
            string q2 = "Quadrant 2";
            string q3 = "Quadrant 3";
            string q4 = "Quadrant 4";

            string negative = "-";
            string rel = "";

            string subx = xAxis.Substring(0, 1);
            string suby = yAxis.Substring(0, 1);

            if (negative != subx && negative != suby){
                rel = q1;
            }
            else if (negative == subx && negative != suby){
                rel = q2;
            }
            else if (negative == subx && negative == suby){
                rel = q3;
            }
            else if (negative != subx && negative == suby){
                rel = q4;
            }

            result_q1.InnerHtml = rel;

        }

        //===============BONUS QUESTION 2===============//
        public static bool IsPrime(int number)
        {
            if (number <= 1){
                return false;
            }
            if (number == 2){
                return true;
            }
            if (number % 2 == 0){
                return false;
            }

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2){
                if (number % i == 0){
                    return false;
                }
            }
            return true;
        }
        public void Button2Clicked(object sender, EventArgs args){
            string input = divisible.Text.ToString();
            int num_input = int.Parse(input);
            string msg = "";

            if (IsPrime(num_input)){
                msg = "The Number is a Prime Number";
            }
            else{
                msg = "The Number is NOT Prime Number";
            }

            result_q2.InnerHtml = msg;
        }

        //===============BONUS QUESTION 3===============//
        public void Button3Clicked(object sender, EventArgs args){
            string input = palindromes_input.Text.ToString().ToLower().Replace(" ", string.Empty);

            char[] charArray = input.ToCharArray();
            Array.Reverse(charArray);

            string s = new string(charArray);
            string result_q2 = "";

            if (input == s){
                result_q2 = "The String is a Palindromes";
            }else{
                result_q2 = "The String is NOT a Palindromes";
            }

            result_q3.InnerHtml = result_q2;
        }
    }
}
