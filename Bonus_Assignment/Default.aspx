﻿<%@ Page Language="C#" Inherits="Bonus_Assignment.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
	<form id="form1" runat="server">
        <!--===============BONUS QUESTION 1===============-->
        <h1>Question 1: Quadrants</h1>
		<asp:TextBox runat="server" id="x_axis" placeholder="X-Axis"></asp:TextBox>
        <!--For some reason when I put my validators the div where I display the result doesnt show. 
            That's why I commented out my validators for now. Please show me how to fix it. Thank you.-->
            
        <!--<asp:RequiredFieldValidator runat="server" Display = "Dynamic" ErrorMessage="Please enter a number" ControlToValidate="x_axis" ID="xAxis_validator" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "x_axis" ID="xAxis_RegularExpressionValidator" ValidationExpression = "^-?[0-9]\d*(\.\d+)?$" 
                                        runat="server" ErrorMessage="Please Enter Valid Number" ForeColor="Red"></asp:RegularExpressionValidator>-->
        <asp:TextBox runat="server" id="y_axis" placeholder="Y-Axis"></asp:TextBox>
        <!--<asp:RequiredFieldValidator runat="server" Display = "Dynamic" ErrorMessage="Please enter a number" ControlToValidate="y_axis" ID="yAxis_validator" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "y_axis" ID="yAxis_RegularExpressionValidator" ValidationExpression = "^-?[0-9]\d*(\.\d+)?$" 
                                        runat="server" ErrorMessage="Please Enter Valid Number" ForeColor="Red"></asp:RegularExpressionValidator>-->
        <asp:Button id="button1" runat="server" Text="Submit" OnClick="Button1Clicked" />
            
        <div runat="server" id="result_q1"></div> 
         
         <!--===============BONUS QUESTION 2===============-->
        <h1>Question 2: Divisible</h1>
        <asp:TextBox runat="server" id="divisible" placeholder="Enter a Number"></asp:TextBox>
       <!-- <asp:RequiredFieldValidator runat="server" Display = "Dynamic" ErrorMessage="Please enter a number" ControlToValidate="divisible" ID="divisible_validator" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "divisible" ID="divisible_RegularExpressionValidator" ValidationExpression = "^[1-9]\d*(\.\d+)?$" 
                                        runat="server" ErrorMessage="Please Enter Valid Number" ForeColor="Red"></asp:RegularExpressionValidator>-->
        <asp:Button id="button2" runat="server" Text="Submit" OnClick="Button2Clicked" />
            
        <div runat="server" id="result_q2"></div> 
            
        <!--===============BONUS QUESTION 3===============-->
        <h1>Question 3: Palindromes</h1>
        <asp:TextBox runat="server" id="palindromes_input" placeholder="Enter a String"></asp:TextBox>
       <!-- <asp:RequiredFieldValidator runat="server" Display = "Dynamic" ErrorMessage="Please enter a string" ControlToValidate="palindromes_input" ID="palindromes_validator" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "palindromes_input" ID="palindromes_RegularExpressionValidator" ValidationExpression = "^[a-zA-Z]+$" 
                                        runat="server" ErrorMessage="Please Enter Valid String" ForeColor="Red"></asp:RegularExpressionValidator>-->
        <asp:Button id="button3" runat="server" Text="Submit" OnClick="Button3Clicked" />
            
        <div runat="server" id="result_q3"></div> 
	</form>
</body>
</html>
